# helloworld-django Project

## :page_with_curl:  _Infrastructure_

The application is deployed on an Elastic Beanstalk Environment. This environment is a single instance environment making use of spot instances. The maximum price per instance-hour, in USD, that is being paid for the Spot Instance is set to the On-Demand price of the instance type being used. The instance type being used is t3.nano and its default price is $0.0052/hr.

## :page_with_curl:  _Project Setup_

All the commands below should be run from a terminal.

Clone the repository

__`❍ git clone https://gitlab.com/vahiwe/helloworld-django.git `__

Move into the directory

__`❍ cd helloworld-django `__
 
Install the necessary python packages

__`❍ pip install -r requirements.txt `__

Start the application 

__`❍ python manage.py runserver `__

Once the application is started, it can be visited at `http://localhost:8000`

If any change or update is made to the source code, the changes can be committed and pushed by using the following commands.

Add changes to staging

__`❍ git add . `__
 
Commit changes

__`❍ git commit -m "commit messages" `__
 
Push changes to the remote repository 

__`❍ git push origin main `__

This will trigger a deployment with the new changes.

## :page_with_curl:  _Deployments_

Deployments to the Beanstalk environment are triggered by a commit to the main branch of the source code repository. Gitlab CI/CD is used as the CI/CD of choice in carrying out the deployment. The steps in the pipeline can be viewed and updated in `.gitlab-ci.yml` file in the repo. If you're making use of a self-hosted gitlab runner, the tag specified when creating the runner needs to be indicated in the job.

For the Gitlab CI/CD to successfully deploy to Elastic Beanstalk some environment variables will need to be set, namely:

* AWS_ACCESS_KEY_ID - AWS access key

* AWS_SECRET_ACCESS_KEY - AWS secret key

* AWS_DEFAULT_REGION - The AWS region code (us-east-1, us-west-2, etc.) of the region containing the Beanstalk Environment 

* AWS_EB_APPLICATION - The name of the Elastic Beanstalk application

* AWS_EB_ENVIRONMENT - Environment name

These environment variables can be set on the repository. 

Below is a screenshot showing where to set these variables.

![Environment Variables](doc/environment-variables.png)

&nbsp;

With these environments set correctly, deployments should be successful as long as the environment exists.


## :page_with_curl:  _Gitlab Runner_

Instead of using the shared runners on Gitlab to run the CI/CD, the gitlab runner can be installed on a local machine and that can be used as a runner. The machine has to be reacheable from the internet for this to work. Using a VM in a cloud provider is preferable. You can visit this [link](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/) for setting up a runner on AWS. To setup a local machine as a runner, you can visit this [link](https://docs.gitlab.com/runner/install/) to view the installation steps for different Operating Systems. Once a new runner has been added it should show up under `Available specific runners`. To specifically make use of your local runner for CI/CD jobs disable shared runners. Also if configuration of gitlab-runner is done, run `gitlab-runner verify` to force it to be connected. Below is a screenshot showing the available runners.

![Available Runner](doc/available-runners.png)

You can visit the application [here](http://django.eba-xwm9f3yp.us-east-2.elasticbeanstalk.com).

__*Happy developing!*__
